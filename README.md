# twitch-nunc

A little Bash script to "follow" Twitch streamers and then check who of them is currently streaming to watch them using Streamlink, all from the comfort of your command line.

## Usage

Check who is live of the channels added to the follow list and get a prompt of who to watch:

`twitch-nunc`

Use the following arguments to edit the channel list (or simply edit ~/.config/twitch-nunc):

```shell
twitch-nunc list               # List added channels
twitch-nunc add <channels>     # Add channels to the list, separated by spaces
twitch-nunc remove <channels>  # Remove channels from the list, separated by spaces
twitch-nunc import <username>  # Import/list follows from your Twitch account
```

Twitch might rate limit you if you check too many channels for their live status in a short amount of time. More channels will also increase the time it takes to perform the check. Adding the 15 channels I would actually watch made the API check take 1 second. Importing all 110 channels I apparently follow with my Twitch account increased the time to 5 seconds.

Channel names you provide can be upper-case or lower-case.

### Examples

Check and watch:

```console
[lily@midna:~]$ twitch-nunc
Asking Twitch for information...
Response received, interpreting...
Done :) These streamers are currently live:

  1: ster | Art | 💀 Ashle Myriad Artstream - new NPC tokens!
  2: Jerma985 | Bloodwash | Very Scary Stuff, DON'T OPEN THE STREAM, SCARE WARNING
  3: SemperVideo | Just Chatting | Computer & Internet Talk
  4: princessxen | Software and Game Development | Writing 😂, a blog engine that you can make your own. Part 1.
  
Type a number to open the stream. Append an 'a' for audio only. Leave empty to cancel.
Selection: 3a█
```

List added channels:

```console
[lily@midna:~]$ twitch-nunc list
These are the 4 channels currently on the follow list:

  ster
  jerma985
  sempervideo
  princessxen
```

Add channels:

```console
[lily@midna:~]$ twitch-nunc add rediculousstreams janlunge ster numquamfideo
The channel 'rediculousstreams' has been added to the list!
The channel 'janlunge' has been added to the list!
The channel 'ster' is already on the list, skipping.
The channel 'numquamfideo' does not exist. Did you mistype?
Done :)
```

Remove channels:

```console
[lily@midna:~]$ twitch-nunc remove rediculousstreams janlunge gronkh
The channel 'rediculousstreams' has been removed from the list!
The channel 'janlunge' has been removed from the list!
The channel 'gronkh' is not on the list, skipping.
Done :)
```

Import channels:

```console
[lily@midna:~]$ twitch-nunc import alilybit
Asking Twitch for information...
Response received, you are following 110 channels:

ster jerma985 sempervideo princessxen [...] (Removed from the example due to its length, but in practice this is a full list)

Do you want to import all of these channels? If you want to manually choose who to add, simply cancel and use the list above as a reference for 'twitch-nunc add'. Pick some of them, or copy the entire list and delete some entries.
Import all channels? [y/N] █
```

## Dependencies

- jq is required to parse Twitch API responses
- Streamlink is recommended if you want to open the streams from the command line, but you can of course modify the script to your liking

## Unimportant Backstory

In the last 7 months, I’ve been really getting into my PinePhone, and it has teached me indescribably much. On a small device with limited computing power, the command line is of course by far the best way to do anything, even more so than on bigger and more powerful devices. The mobile Twitch website does unfortunately not even allow logging in and just tells you to install the Android/iOS app. Fortunately, this, along with wanting to watch YouTube from the terminal, made me discover [Streamlink](https://streamlink.github.io/) and mpv. There is also a [GUI client for Twitch+Streamlink](https://github.com/streamlink/streamlink-twitch-gui), and a [third-party YouTube GUI client](https://freetubeapp.io/). But especially on my PinePhone, I want to use the terminal as much as possible. I could open streams in the terminal using Streamlink, but to check who is online I would have to manually attempt to watch channels, and each attempt takes a bit. So I made this, since I haven’t found any existing CLI tool that allows checking selected streams for their live status. I don’t have anything set up to check my YouTube subscriptions from the terminal yet, but RSS feeds should make that simple.

At first I had named this "twitch-cli". But apparently that already exists, and is something from Twitch itself. I couldn’t think of any good name while I was coding most of the initial version, but at 3 am when I finished the checking and watching parts, I had the idea to use the Latin word for "now": nunc. Which is of course a reference to Twitch Now. By the way, if you also want to use Twitch Now, which is as of now (2022-05) broken due to the new Twitch API, use either [this patched version](https://github.com/Ndragomirov/twitch-now/pull/267#issuecomment-1049247939) or possibly [this other addon (Gumbo)](https://github.com/Seldszar/Gumbo) (I haven’t used that one yet).

## todo

- rewrite in python
- config file for things like what player/command to use
- proper auth with personal account or for those who don't want that slow web crawl
- add major feature to list and view vods because as far as I know there is no tool other than the slow bloated website to list vods and i really want this
- desktop notifications
